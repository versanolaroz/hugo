
LOGBOEK

> *Maandag 18 september 2017*

Vandaag zijn we in de ochtend begonnen met onze project presentatie, ik moet hierbij zeggen dat ik heel trots ben op het team. Met minimale voorbereiding (wegens tijdsgebrek) hebben we het heel goed getroffen, door de planning die ik had gemaakt wist elk teamgenoot wat die moest doen en zeggen tijdens de presentatie. Tevens had ik ook de presentatie gemaakt die volgens mijn teamgenoten goed uit zag. 
Als ik onze presentatie moet vergelijken met de rest was die van ons het meest duidelijk, na de presentatie hadden we op na 1 student geen vragen. 

Helaas hebben we na de presentatie het connect. project afgesloten, we zijn vandaag begonnen met een nieuw project waarvoor we wel een beoordeling gaan krijgen. Dit project gaan we zeer serieus nemen, we zijn al begonnen met een taakverdeling en we hebben elkaar belooft op tijd alles te maken zodat we niks last minute hoeven in te leveren.

Na de Design Challenge hebben we weer een workshop gehad over bloggen, nog steeds vind ik de websoftware Gitlab niet handig om te gebruiken. Het is na 1 week nog steeds onduidelijk, ik hoop dat ik het snel ga snappen..

Tevens hebben we vandaag gelunched op school ter afsluiting van de eerste literatie, het was gezellig en de sfeer zat er goed in!

