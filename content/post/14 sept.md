LOGBOEK

> *Donderdag 14 september 2017*

Vandaag heb ik een werkcollege gevolgd over het onderwerp: Verbeelden
Uiteraard heb ik uitgekeken daar dit werkcollege omdat ik de hoor college zeer interessant vond.
De dag begon met een spel waarbij de docent een figuur op noemde die je abstract moest tekenen doormiddel van lijnen en cirkels.  Het spel vond ik best uitdagend omdat tekenen niet mijn beste vak is en ik er eigenlijk weinig van bak, verrassend genoeg is het toch goed gegaan. In het algemeen had de groep waarmee ik zat dezelfde vormen en figuren, grappig om te zien dat je dan uit zoveel ideeen om iets te tekenen zovaak hetzelfde kan tekenen. 

Na het spel gingen we door met het een opdracht waarbij we afbeeldingen (die we moesten meenemen) moesten verbeelden. De opdracht was leuk, interessant en zeker een plus punt omdat je zo de stof beter snapt doordat je het toepast. Ik zelf vond het nogal een uitdaging, vond het lastig om de juiste benaming ervoor te vinden maar gelukkig kregen we een spiekbrief waar we inspiratie uit konden halen. 

In de middag na het hoorcollege ben ik weer aan de slag gegaan met het onderzoeks dossier van de design challenge, mijn andere teamgenoten zijn aan de slag gegaan met het prototype. Vandaag is alles samengekomen en het ziet er goed uit, morgen gaan we de finishing touches eraan toevoegen en vervolgens inleveren. Tevens begin ik ook morgen met de presentatie voor aanstaande maandag, spannend.. Ben zelf geen presenatie freak en het is zeker nog iets waar ik hard aan moet werken maar ik geloof dat hoe meer ik het doe hoe makkelijker het uiteindelijk zal worden.

