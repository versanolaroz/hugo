LOGBOEK

> *Dinsdag 5 september 2017*

Vandaag hebben het hoorcollege: Design Theory gevolgd. Het was een zeer interessante hoorcollege waarbij ik nieuwe dingen heb geleerd zoals het verschil in design en kunst (besef moment). 

**Mijn ervaring**

Tegenwoordig zie je design en kunst overal komt, in elke stad of plek waar je bent is er wel iets van te zien. Vanaf 2009 houd ik mezelf al bezig met design en door de jaren heen zag ik design eigenlijk als alles wat KUNSTmatig is, in principe klopt de vergelijking ook maar voor mezelf had ik nooit gedacht dat design & kunst in theorie zoveel verschillen. Ik dacht daar goed over na en vroeg mezelf de vraag wat ik eigenlijk ben; een kunstenaar of een designer? 

Naast school ben ik veel bezig met design, van eigen covers designen tot websites bouwen/vormgeven. Naast dat ben ik ook een muziekant, ik produceer al jaren muziek voor mezelf als voor mensen. Dus zoals je misschien al kunt opmaken uit mijn verhaal, ik ben voor mezelf (intern/subjectief) als  daar buiten (objectief) bezig, kan ik mezelf dus een designer of kunstenaar noemen? In dit geval kan ik zeggen dat ik een designer ben EN een kunstenaar ben.

Naast de interessante hoor college hebben we gebrainstormd over de naam van de app met als resultaat: Connect. 
Waarom Connect? 
Als groep hebben we een tijd zitten twijfelen over wat het doel van de applicatie is/kan zijn, om daar achter te komen hebben ons zelf deze vragen gesteld:

Waarom zou een school de applicatie gebruiken?
* Omdat de applicatie in de introductie week gebruikt kan worden als een tool die studenten helpt om een makkelijkere start te maken (de eerste week) omdat ze al bekend zijn met de omgeving en mede-studenten. Dit kan uiteindelijk tot betere prestatie's leiden op langer termijn, wat goed is voor de student als voor de school.

Wat is het doel van de app (concreet, waarom maken wij de app?)
* Om eerste jaars studenten van de opleiding CMD door diverse activiteiten elkaar als de stad Rotterdam leren kennen.

In het algemeen kan je hieruit leiden dat het belangrijk is dat de studenten met elkaar kennis maken, en wat doe je dan? In het Engels; Connecten. 