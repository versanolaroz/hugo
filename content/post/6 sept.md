LOGBOEK

> *Woensdag 6 september 2017*

Vandaag hebben we een interessante Design Challenge gedaan waarbij we in het verleden een bestaande situatie hadden omgezet in een gewenste situatie. Het doel was om meer inzicht te krijgen in het ontwerpproces van CMD.

Een ontwerpproces bestaat uit 3 delen; het onderzoek, het concept en het prototype. Dat zijn aspecten die we moesten meenemen en onderzoeken in de opdracht. De opdracht was individueel (ieder zijn eigen situatie), ik zelf koos voor het moment dat ik mijn eigen feest had georganiseerd. 
In eerste instantie dacht ik dat mijn situatie niet relevant omdat je het niet direct een ontwerp kan noemen maar als je kijkt naar de processen past mijn situatie daadwerkelijk wel in het plaatje en tevens stond het als voorbeeld op de lesbrief.

Stap 1: Onderzoek
Als je een feest organiseerd zijn er veel aspecten waar je rekening mee moet houden zoals:
- Thema
- Kleugebruik
- Designs
- Locatie
- Zaal bekleding (onderdeel van het thema)
- Capaciteiten/doelgroep

Stap 2: Concept
Als het onderzoek duidelijk is ga je door met het neerzetten van het concept, in dit geval kan je bij het onderzoek haast geen fouten maken omdat je een feest in 1x goed moet organiseren anders kost het je heel veel geld. In mijn situatie was mijn concept resultaat heel dichtbij het prototype maar kon het nog goed onderscheiden.

Club bezoeken en het onderzoek/idee voorleggen
Flyer samples bestellen
Extra spullen en assosiarissen bestellen

Stap 3: Prototype
Dit was voor mij de eindfase waarin ik overging in de volgende stappen;
Massa productie van flyser en posteres
Start social media promotie

Naast dit werkcollege hebben we een start gemaakt met het onderhouden van het logboek en het schrijven in Stackedit. Het programma Stackedit vind ik een prima tool om snelle logs te schrijven en op te slaan in formats die leerbaar is voor website platformen zoals gitlab. Had het snel door en begon direct met het schrijven van mijn dagelijkse logs.