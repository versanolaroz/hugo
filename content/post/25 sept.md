
LOGBOEK
> *Maandag 25 september 2017*

Vandaag heb ik gewerkt een applicatie concept wat ik heb verzonnen, de opdracht was/is om 3 concepten als team te verzinnen en er 1 uiteindelijk te kiezen en verder uit te werken. Mijn concept idee ging als volgt:

Search & Destroy is een spelconcept wat gebaseerd is op de game Call Of Duty, het spel speel je door middel van het zoeken van een bepaald pakket die je moet uitschakelen. Wij zijn als team gaan brainstormen over hoe we het spel kunnen twisten naar een game die gespeeld kan worden in de introductieweek. 

Het spel gaat als volgt; alle eerstejaars cmd studenten (doelgroep) krijgen via de applicatie een random naam toegewezen, dit is een naam van één van de x aantal cmd studenten. Na dat iemand een naam toegewezen heeft gekregen is het de bedoeling dat de speler die persoon gaat zoeken en uitschakelen, aangezien iedereen nieuw is kent niemand elkaar nog. Uit ervaring weet ik dat de speler aan diverse mensen gaat vragen of ze de target kennen die hij/zij aan het zoeken is om zo dichterbij diegene te komen. 

Als je in de buurt van je target bent geeft de app door middel van een meter aan hoe ver je bent verwijderd, zo weet je dat je in de buurt bent. Eenmaal binnen een straal van 5 meter wordt de functie; stay to destroy target geactiveerd, hiermee moet je een x aantal seconden binnen die 5 meter blijven zodat je de target kan vernietigen (destroyen). LET OP: op dit moment krijgt de target een bericht dat hij/zij wordt ‘aangevallen’. De target kan de aanval stoppen of voorkomen door middel van het verlaten van het 5 meter-gebied. Als dit lukt wordt het spel alleen maar spannender omdat je eindelijk weet wie je zoekt en eigenlijk altijd op de uitkijk bent. 


