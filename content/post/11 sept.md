LOGBOEK

> *Maandag 11 september 2017*

Vandaag hebben we keikard gewerkt aan het onderzoek van de applicatie (conncect). Als groep hadden we allemaal concepten bedacht en ideeen hoe de applicatie er uiteindelijk zou gaan zien maar we misten nog heel stuk aan onderzoek, onderzoek wat eigenlijk bepaalde hoe we de concepten zouden maken.

We hebben ons dus vandaag tijdens de Designs Challenges verdiept in het onderzoek, daarbij hebben de onderstaande punten onderzocht:

Doelgroep
Opleiding
Doel
Regels
Keuzes
Magic Circle
Meaningful Play
Rolverdeling en uitleg

Naast alle punten die besproken moesten worden hebben we als groep individuele interviews gehouden met diverse studenten. De vragen waren gebaseerd op de enquete die we hadden gemaakt. Met de antwoorden kunnen we een concept idee schetsen van de applicatie en hoe de CMD studenten het graag zouden willen hebben.

Naast het onderzoek hadden we ook weer een interessante workshop op het platform Gitlab wat ons gaat hosten. Persoonlijk vind ik Gitlab niet een overzichtelijk programma voor iemand die er nog meer mee heeft gewerkt. Desondanks het tutorial was het nog steeds uitdagend om de stappen goed te doorlopen. Gitlab zou ik naast school niet uit mezelf gaan gebruiken en zeker niet aanraden.
