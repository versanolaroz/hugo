LOGBOEK

> *Woensdag 13 september 2017*

Vandaag heb ik samen met mijn groep keihard gewerkt aan het onderzoek voor de applicatie. Het originele idee wat ik in mijn vorige logboeken heb beschreven is veranderd naar iets vernieuwds doormiddel van feedback van de docent. In eerste instantie vond ik het eerste idee goed maar het was niet goed samen te voegen met het thema (architectuur) wat wij hebben. Het idee veranderd naar het volgende:

Het concept van de game is dat eerstejaars cmd studenten in contact komen met elkaar door middel van samen het spel te moeten spelen (4 personen) Je gaat naar verschillende locaties (gevarieerde architecten gebouwen) en ontgrendeld vragen over de gebouw die je binnen een bepaalde tijd moet beantwoorden. Als je dit goed hebt beantwoord krijg je hele team punten, zo niet heb je gefaald en moet je door naar een ander locatie voor nieuwe vragen. Je kunt deze punten inwisselen voor verschillende producten/deals ( mcdonald’s deals, bioscoopticket, museum entrance, gym entrance) Verder is er ook nog om de zoveel tijd double points beschikbaar bij bepaalde locaties, deze zijn 1 keer geldig. Dus zorg dat je sneller bent dan andere teams.

Daarnaast hebben we ook gewerkt aan het logo + interface designs... Het was best uitdagend omdat we een compleet nieuw idee moesten uitwerken in een korte tijd met een prototype. Het logo is gebaseerd op de kleuren rood en blauw. Rotterdam staat voornamelijk bekend om die kleuren, daarbij kan je denken aan: water, haven en Feyenoord.