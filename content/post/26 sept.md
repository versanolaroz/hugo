
LOGBOEK
> *Dinsdag 26 september 2017*

Vandaag hebben een hoorcollege over onderzoeken gekregen, deze hoor college was heel pittig. Er werden veel technieken uitlegd waar de docent vervolgens nog dieper in dook. In het algemeen was het nog wel te volgen maar alles ging te snel om en uiteindelijk was ik zelf de draad kwijt. Weinig aantekeningen gemaakt door een overload aan informatie. Dit hoorcollege wil ik later terug bekijken en op eigen tempo alles zelf gaan besturen want het is heel interessant als je het snapt.

Verder heb ik vandaag gewerkt aan mijn concept idee, heb diverse visuele aspecten ontworpen en extra gedocumenteerd om het plaatje nog duidelijker te maken. Morgen gaan we het uiteindelijke concept kiezen die we tot in detail gaan uitwerken, kijk er zeer naar uit omdat ik persoonlijk denk dat het die van mij zal zijn en aangezien ik er al aan heb gewerkt scheelt het tijd & werk en kunnen we ons op school focussen op andere prioriteiten.