LOGBOEK

> *Dinsdag 12 september 2017*

Vandaag hebben we in de ochtend een hoor college design theory gekregen. Daarbij stond verbeelden centraal als onderwerp. Verbeelden is zeer intressant en breed onderwerp om over te praten of na te denken. 
De docent heeft het gehad over de algemene technieken om een afbeelding visueel uit te werken doormiddel van het toepassen van verschillende theorien.

In het dagelijks leven komen we veel van alles tegen wat een boodschap met zich meebrengt, met de besproken theorien heb ik geleerd hoe ik bepaalde kunstwerken of afbeeldingen kan ontleden en eigenlijk de reden tot creeeren van de afbeelding kan achterhalen. Daarbij heb 3 communicatie functies: zien, begrijpen en overtuigen. Het is leuk omdat ik nu anders naar bepaalde 'beelden' kan kijken en er eigenlijk dieper over kan nadenken met de theorie die ik heb geleerd vandaag. Ik zie er naar uit om aan de slag te gaan met het werk college en nog meer te leren over 'verbeelden'.

Vandaag heb ik ook in mijn eigen vrije tijd gewerkt aan ons design challenge opdracht. Ik ben aan de slag gegaan met het onderzoek en heb heel het onderzoeks document beter uitgewerkt met duidelijke analyses. Daarnaast heb ik ook een start gemaakt aan het prototype, we hebben schetsen gemaakt van de applicatie die we deze week gaan uitwerken tot een paper-prototype.