LOGBOEK

> *Maandag 4 september 2017*

Vandaag zijn we officieel begonnen aan de opleiding, daarbij ook met de brainstorm sessie van de Design Challenge. Als team hebben we diverse concepten/ideeën besproken over hoe de app eruit zou kunnen zien. Natuurlijk hebben we elementen die een game maken centraal gehouden (doel, regels, keuzes).

Het doel van de applicatie:
- Studenten door diverse activiteiten in Rotterdam elkaar beter leren kennen en de stad zelf. 

Regels in de applicatie: 
- De groepen de er gemaakt worden voor de uitjes mogen minimaal 2 en maximaal 6 personen bevatten. 
- De groepen indeling is tijdgebonden.
- Maximaal 2 uitjes per dag (na het attenden van een groep is het niet meer mogelijk om binnen 5 uur een ander uitje te joinen tenzij je de eerste canceld).

Keuzes in de applicatie: 
Bij het starten van de applicatie (als je al ingelogd bent) zul je 4 catogerien zien; Evenementen, sport, eten en bezienswaardigheden. Eenmaal een catogerie geselecteerd zul je zien wat er in de buurt te doen specifiek uit die catogerie. Verder zul je de mogelijkheid hebben om te kiezen aan welke je activiteit je op dat moment wilt deelnemen en volgens je groep. Je krijgt onder elke activiteiten ronde foto's te zien van de personen die er al in zitten zodat je een idee hebt welke groep mensen je in de groep komen. Verdere keuzes zitten nog in de brainstormsessie. 

*Interface > Inloggen (Facebook) > Interesses > Activiteit > Groepen/vrienden kiezen > Chat > **CONNECT!***

Ik zelf vond de brainstorm heel interessant omdat je jezelf uitdraagt alles uit jezelf tehalen om iets te creeeren/bedenken wat er nog niet is. Als team vind ik dat we zeer sterk voor staan omdat we allemaal andere talenten hebben en we eigenlijk goed op elkaar kunnen afstemmen wie/wat doet. 

Potitieve punten
We konden het goed met elkaar vinden, iedereen wist wat die moest doen en we werkten vlot door.
Tevens hadden hele toffe ideeen voor de app, de brainstormsessie was heel 'useful'.

Negatieve punten
Uiteindelijk waren we na een paar uur de concentratie kwijt en die ook niet meer terug, voor mijn gevoel hebben we daar veel kostbare tijd verloren die we goed konden gebruiken om bijv. al schetsen te maken van de applicatie of andere taken. 
